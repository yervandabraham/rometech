<?php

namespace Rometech\CategoryWidget\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Setup\EavSetup $eavSetup
     */
    private $eavSetup;

    /**
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->entityType = \Magento\Catalog\Model\Category::ENTITY;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $this->eavSetup->addAttribute(
            $this->entityType,
            'is_featured',
            [
                'type' => 'int',
                'label' => 'Featured Category',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'default' => 0,
                'sort_order' => 101,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
        $this->eavSetup->addAttribute(
            $this->entityType,
            'featured_order',
            [
                'type' => 'int',
                'label' => 'Featured Order',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'default' => 0,
                'sort_order' => 102,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
        $this->eavSetup->addAttribute(
            $this->entityType,
            'featured_color',
            [
                'type' => 'varchar',
                'label' => 'Featured Color',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'default' => 0,
                'sort_order' => 103,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );

        $setup->endSetup();
    }
}
