<?php
namespace Rometech\CategoryWidget\Cron;

class Qty
{
    protected $_stockRegistry;
    protected $_logger;
    protected $_fileCsv;
    protected $_directoryList;


    public function __construct(
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\File\Csv $fileCsv
    )
    {
        $this->_stockRegistry = $stockRegistry;
        $this->_logger = $logger;
        $this->_directoryList = $directoryList;
        $this->_fileCsv = $fileCsv;
    }

    public function execute()
    {
        $this->_logger->info('Renew qty cron started!');

        $file  = $this->_directoryList->getPath('var').'/import/linnworks.csv';
        $this->_logger->info("Reading " . $file);

        if (file_exists($file)) {
            $data = $this->_fileCsv->getData($file);
            for($i=1; $i<count($data); $i++) {
                try {
                    $stockItem = $this->_stockRegistry->getStockItemBySku($data[$i][0]);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
                    $stockItem = false;
                }
                if($stockItem && ($stockItem->getQty() != $data[$i][1]) && ($data[$i][1] != "")){
                    $stockItem->setQty($data[$i][1]);
                    $stockItem->setIsInStock((bool)$data[$i][1]);
                    $this->_stockRegistry->updateStockItemBySku($data[$i][0], $stockItem);
                    $this->_logger->info("Sku ".$data[$i][0]." updated with qty=".$data[$i][1]);
                }
//                else{
//                    $this->_logger->info("Sku ".$data[$i][0]." skipped!");
//                }
            }
        }
        $this->_logger->info('Renew qty cron finished!');
    }
}